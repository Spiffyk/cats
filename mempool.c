#include <string.h>

#include "utils.h"

#include "mempool.h"

/** A memory pool. */
struct mempool_pool {
	size_t num_items; /**< Number of items currently allocated in the pool. */
	struct mempool_pool *next; /**< The next pool in the linked list. */
	uint8_t data[]; /**< Mempool data - bitmap and allocatable items. */
};

/** A memory-pool-based allocator. */
struct mempool {
	size_t pool_size; /**< Number of items that can be allocated by a pool in this allocator. */
	size_t item_size; /**< Size of a single item in the allocator. */
	size_t allocated_items; /**< Number of currently allocated items. */
	size_t allocated_pools; /**< Number of currently allocated pools. */
	mempool_destructor_cb destructor; /**< Item destructor function. */
	struct mempool_pool *pool; /**< The pool data. */
};


static inline size_t bitmap_size(size_t pool_size)
{
	size_t min_bytes = CEIL_DIV(pool_size, 8);
	return CEIL_DIV(min_bytes, sizeof(size_t)) * sizeof(size_t);
}

static inline size_t pool_data_size(size_t pool_size, size_t item_size)
{
	return bitmap_size(pool_size) + (item_size * pool_size);
}

static inline uint8_t *pool_bitmap(struct mempool_pool *pool)
{
	return pool->data;
}

static inline uint8_t *pool_data(const struct mempool *mempool,
                                 struct mempool_pool *pool)
{
	return &pool->data[bitmap_size(mempool->pool_size)];
}

static inline struct mempool_pool *pool_create(struct mempool *mempool)
{
	return calloc(1, sizeof(struct mempool_pool) +
			pool_data_size(mempool->pool_size, mempool->item_size));
}

static void pool_free(struct mempool_pool *pool)
{
	while (pool) {
		struct mempool_pool *next = pool->next;
		free(pool);
		pool = next;
	}
}

static bool item_is_in_pool(const struct mempool *mempool,
                            const struct mempool_pool *pool,
                            const void *item)
{
	const uint8_t *pb = (uint8_t *) pool;
	const uint8_t *pe = pb + sizeof(*pool) + pool_data_size(
			mempool->pool_size, mempool->item_size);

	const uint8_t *item_pos = item;
	return item_pos >= pb && item_pos < pe;
}


struct mempool *mempool_create(size_t pool_size, size_t item_size,
                               mempool_destructor_cb destructor)
{
	struct mempool *mempool = malloc(sizeof(struct mempool) +
			pool_data_size(pool_size, item_size));
	REQUIRE(mempool);

	mempool->pool_size = pool_size;
	mempool->item_size = item_size;
	mempool->allocated_items = 0;
	mempool->allocated_pools = 0;
	mempool->destructor = destructor;
	mempool->pool = pool_create(mempool);

	return mempool;
}

static bool mempool_item_destroy(void *item, void *baton)
{
	struct mempool *mempool = baton;
	mempool->destructor(item);
	return true;
}

void mempool_destroy(struct mempool *mempool)
{
	if (mempool->destructor)
		mempool_apply(mempool, mempool_item_destroy, mempool);

	pool_free(mempool->pool);
	free(mempool);
}

static struct mempool_pool *find_nonfull_pool(struct mempool *mempool)
{
	struct mempool_pool **pool = &mempool->pool;
	do {
		if (!*pool) {
			mempool->allocated_pools++;
			*pool = pool_create(mempool);
			return *pool;
		}

		if ((*pool)->num_items >= mempool->pool_size) {
			pool = &(*pool)->next;
			continue;
		}

		return *pool;
	} while (true);
}

void *mempool_alloc(struct mempool *mempool)
{
	struct mempool_pool *pool = find_nonfull_pool(mempool);
	uint8_t *bm = pool_bitmap(pool);

	for (size_t i = 0; i < mempool->pool_size; i++) {
		if (bm[i / 8] == 0xFF) {
			i += 7;
			continue;
		}
		if (test_bit(bm, i))
			continue;

		pool->num_items++;
		mempool->allocated_items++;
		set_bit(bm, i);
		void *result = &pool_data(mempool, pool)[i * mempool->item_size];
		memset(result, 0, mempool->item_size);
		return result;
	}

	assert(false);
	return NULL;
}

void mempool_free(struct mempool *mempool, void *item)
{
	struct mempool_pool *pool = mempool->pool;
	while (pool) {
		if (!item_is_in_pool(mempool, pool, item)) {
			pool = pool->next;
			continue;
		}

		uint8_t *item_pos = item;
		size_t offset = item_pos - pool_data(mempool, pool);
		assert(offset % mempool->item_size == 0);

		size_t i = offset / mempool->item_size;
		uint8_t *bm = pool_bitmap(pool);
		if (test_bit(bm, i)) {
			if (mempool->destructor) {
				void *item = &pool_data(mempool, pool)[i * mempool->item_size];
				mempool->destructor(item);
			}
			reset_bit(bm, i);
			pool->num_items--;
			mempool->allocated_items--;
		} else
			assert(false);

		return;
	}

	assert(false);
}

void mempool_apply(struct mempool *mempool, mempool_apply_cb cb, void *ctx)
{
	size_t bmsize = bitmap_size(mempool->pool_size);
	struct mempool_pool *pool = mempool->pool;
	while (pool) {
		uint8_t bm[bmsize];
		memcpy(bm, pool_bitmap(pool), bmsize);

		uint8_t *data = pool_data(mempool, pool);
		for (size_t i = 0; i < mempool->pool_size; i++) {
			if (!test_bit(bm, i))
				continue;

			if (!cb(&data[i * mempool->item_size], ctx))
				return;
		}

		pool = pool->next;
	}
}

extern inline size_t mempool_count_items(struct mempool *mempool)
{
	return mempool->allocated_items;
}

extern inline size_t mempool_count_pools(struct mempool *mempool)
{
	return mempool->allocated_pools;
}
