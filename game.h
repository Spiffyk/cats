#pragma once

#include <stdint.h>

#include "mempool.h"
#include "utils.h"
#include "vec.h"


// Entities ////////////////////////////////////////////////////////////////////

typedef uint64_t entity_id_t;
#define ENTITY_ID_NULL 0

// Components //////////////////////////////////////////////////////////////////

#define MAX_COLLISIONS 32

enum collider_type {
	COLLIDER_NULL = 0,
	COLLIDER_CIRCLE
};

struct phys {
	struct entity *owner; /**< Pointer to the owner of this phys. */
	v2f pos; /**< Entity position */
	v2f vel; /**< Entity velocity */
	v2f force; /**< Force applied to the entity */
	float mass; /**< Mass of the entity */
	float drag; /**< Drag coefficient - not the actual physical one,
	             * just some number that works nicely for the game. */

	struct {
		enum collider_type type; /**< Determines the validity of union
		                          * members. */
		union {
			/** Valid for `_CIRCLE`. */
			struct {
				float radius;
			} circle;
		};

		uint16_t num_cols; /**< Number of current collisions. */
		entity_id_t cols[MAX_COLLISIONS]; /**< Entities with which this
		                                   * one is colliding. */
	} col;
};


// Entities ////////////////////////////////////////////////////////////////////

enum entity_type {
	ENTITY_TYPE_NULL = 0,
	ENTITY_TYPE_CAT,
};

struct entity_base {
	enum entity_type type;
	entity_id_t id;
	bool destroyed;
};

struct cat {
	struct entity_base base;
	uint8_t paw;
	float time_to_paw;

	v2f ppos;
	struct phys *phys;
	v3f color;
	float angle;
};

struct entity {
	union {
		struct entity_base base;
		struct cat cat;
	};
};


// Particles ///////////////////////////////////////////////////////////////////

enum particle_type {
	PARTICLE_TYPE_NULL = 0,
	PARTICLE_TYPE_PAW,
};

struct particle {
	enum particle_type type;
	v2f pos;
	v2f vel;

	uint64_t age;     /**< in milliseconds */
	uint64_t age_max; /**< in milliseconds */
};


// Game ////////////////////////////////////////////////////////////////////////

struct game {
	uint64_t time;
	struct mempool *phys;
	struct mempool *cats;
	struct mempool *particles;

	struct cat *player_cat;

	v2f world_mouse;
	struct {
		v2f pos;
		float scale;
	} camera;
};

extern struct game game;


int game_init(void);
void game_deinit(void);

void game_update(uint64_t delta);
