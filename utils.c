#include <limits.h>
#include <stdio.h>
#include <string.h>

#ifdef __WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

#include "SDL2/SDL_error.h"

#include "utils.h"


// Logging /////////////////////////////////////////////////////////////////////

inline int print_sdl_error(const char *title)
{
	return ERRF("%s: %s\n", title, SDL_GetError());
}

// Allocating sprintf (GNU-like) ///////////////////////////////////////////////

int xasprintf(char **strp, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	int len = vxasprintf(strp, fmt, ap);
	va_end(ap);
	return len;
}

int vxasprintf(char **strp, const char *fmt, va_list ap)
{
	va_list ap2;
	va_copy(ap2, ap);
	int len = vsnprintf(NULL, 0, fmt, ap);
	if (len <= 0) {
		*strp = NULL;
		return 0;
	}

	*strp = malloc(len + 1);
	REQUIRE(*strp);
	vsnprintf(*strp, len + 1, fmt, ap2);
	return len;
}


// Filesystem //////////////////////////////////////////////////////////////////

const char *program_exe_path(void)
{
#ifdef __EMSCRIPTEN__
	return NULL;
#else
	static bool inited = false;
	static char buf[256];

	if (inited)
		return buf;

#ifdef __linux__
	ssize_t bytes = readlink("/proc/self/exe", buf, sizeof(buf));
	assert(bytes && bytes < sizeof(buf));
	buf[bytes] = '\0';
#elif __WIN32
	DWORD bytes = GetModuleFileNameA(NULL, buf, sizeof(buf));
	assert(bytes && bytes < sizeof(buf));
#else
#error Unsupported operating system
#endif // __linux__ / __WIN32

	inited = true;
	return buf;
#endif // __EMSCRIPTEN__
}

const char *program_dir_path(void)
{
#if __EMSCRIPTEN__
	return "";
#else
	static bool inited = false;
	static char buf[256];

	if (inited)
		return buf;

	const char *exe_path = program_exe_path();
	strncpy(buf, exe_path, sizeof(buf));
	char *c = strrchr(buf, '/');
	if (!c) c = strrchr(buf, '\\');
	assert(c);

	c++;
	*c = '\0';

	inited = true;
	return buf;
#endif // __EMSCRIPTEN__
}

char *program_file(const char *relpath)
{
	char *result;
	xasprintf(&result, "%s%s", program_dir_path(), relpath);
	return result;
}

char *read_file_string(const char *path)
{
	FILE *f = fopen(path, "r");
	if (!f)
		return NULL;

	int err = fseek(f, 0L, SEEK_END);
	if (err)
		return NULL;
	long length = ftell(f);

	char *result = malloc(length + 1);
	if (!result)
		goto exit;

	rewind(f);
	size_t read = fread(result, 1, length, f);
	if (read != length) {
		free(result);
		return NULL;
	}
	result[length] = '\0';

exit:
	fclose(f);
	return result;
}


// Time ////////////////////////////////////////////////////////////////////////

uint64_t clock_millis(clockid_t clock)
{
	struct timespec ts;
	clock_gettime(clock, &ts);
	uint64_t result = ts.tv_nsec / 1000000;
	result += ts.tv_sec * 1000;
	return result;
}

uint64_t clock_usec(clockid_t clock)
{
	struct timespec ts;
	clock_gettime(clock, &ts);
	uint64_t result = ts.tv_nsec / 1000;
	result += ts.tv_sec * 1000000;
	return result;
}


// Strings /////////////////////////////////////////////////////////////////////

int sstrncmp(const char *a, size_t a_len, const char *b, size_t b_len)
{
	if (a_len < b_len)
		return -1;
	if (a_len > b_len)
		return 1;
	return strncmp(a, b, a_len);
}


// Misc ////////////////////////////////////////////////////////////////////////

size_t popcnt_bits(const uint8_t *bitmap, size_t len)
{
	size_t result = 0;
#ifdef __GNUC__
	for (size_t i = 0; i < len; i++) {
		result += __builtin_popcount(bitmap[i]);
	}
#else
	for (size_t i = 0; i < (len * CHAR_BIT); i++) {
		result += test_bit(bitmap, i);
	}
#endif
	return result;
}
