#pragma once

#include <stdbool.h>
#include <stdlib.h>

/** A memory-pool-based allocator. The allocator manages contiguous memory
 * pools of same-sized items. When a pool is full, a new pool is allocated
 * to accomodate for new item allocations. */
struct mempool;

/** Callback function prototype for `mempool_apply()`. The return value
 * determines whether iteration should continue - `true` for continuation,
 * `false` for break. */
typedef bool (*mempool_apply_cb)(void *item, void *baton);

/** Callback function prototype for `mempool_free()`. Used to clean up an
 * item before it is freed. */
typedef void (*mempool_destructor_cb)(void *item);


/** Creates a new pool allocator for items of size `item_size`. Each pool
 * of this allocator has a capacity of `pool_size` items.
 * `destructor` may be `NULL`. */
[[nodiscard]]
struct mempool *mempool_create(size_t pool_size, size_t item_size,
                               mempool_destructor_cb destructor);

/** Destroys the specified pool allocator, including all of its pools. */
void mempool_destroy(struct mempool *mempool);

/** Allocates a new item in the specified allocator and returns it.
 * The memory contents will always be intialized to zero. */
[[nodiscard]]
void *mempool_alloc(struct mempool *mempool);

/** Frees the specified `item` so that the memory may be reused for another
 * allocation. The `item` must belong to the specified `mempool`. */
void mempool_free(struct mempool *mempool, void *item);

/** Iterates through all items allocated in the specified `mempool` allocator,
 * calling `cb` on each one. `ctx` is passed through to `cb`, unmodified.
 * Iteration stops when the last item has been processed, or when the callback
 * returns `false`. */
void mempool_apply(struct mempool *mempool, mempool_apply_cb cb, void *ctx);

/** Gets the number of all items allocated in the specified `mempool`. */
[[nodiscard]]
size_t mempool_count_items(struct mempool *mempool);

/** Gets the number of pools in the specified `mempool` allocator. */
[[nodiscard]]
size_t mempool_count_pools(struct mempool *mempool);
