#include "SDL2/SDL.h"
#include "GL/glew.h"

#include "game.h"
#include "input.h"
#include "utils.h"
#include "vec.h"

#include "pres.h"

enum texture_ix {
	TEX_NULL = 0,

	TEX_COUNT
};

enum array_ix {
	VAO_NULL = 0,

	VAO_RECTS,

	VAO_COUNT
};

enum buffer_ix {
	VBO_NULL = 0,

	VBO_RECTS_VERTS,
	VBO_RECTS_INSTS,

	VBO_COUNT
};

enum shader_ix {
	SHD_NULL = 0,

	SHD_RECT,

	SHD_COUNT
};

struct shader {
	GLuint id;
	GLint gu_mvp;
	GLint gu_time;
};

struct bo_rect {
	v2f pos;
	v2f dims;
	GLfloat rot;
	v4f col;
};

static const v2f rect_vertices[] = {
	{ { -1, -1 } },
	{ { -1, +1 } },
	{ { +1, -1 } },
	{ { +1, +1 } },
};

static const GLubyte rect_indices[] = { 3, 0, 1, 2, 0, 3 };


struct pres_i {
	int init_stage;

	SDL_Rect windowed_bounds;
	SDL_Window *window;
	SDL_GLContext *context;
	bool surface_inited;

	GLuint arrays[VAO_COUNT];
	GLuint buffers[VBO_COUNT];
	struct shader shaders[SHD_COUNT];
	ARRAY(const char *) shader_libs;
	ARRAY(int) shader_lib_lengths;

	ARRAY(struct bo_rect) rects;
};

struct pres pres;
static struct pres_i pres_i;


static void destroy_main_surface(void)
{
	// TODO
}

static void ortho(GLfloat *arr, float left, float right, float top,
                  float bottom, float near, float far)
{
	float cx =  2.0 / (right - left);
	float cy =  2.0 / (top - bottom);
	float cz = -2.0 / (far - near);
	float tx = -(right + left) / (right - left);
	float ty = -(top + bottom) / (top - bottom);
	float tz = -(far + near) / (far - near);

	memset(arr, 0, sizeof(GLfloat[16]));
	arr[0] = cx;
	arr[3] = tx;
	arr[5] = cy;
	arr[7] = ty;
	arr[10] = cz;
	arr[11] = tz;
	arr[15] = 1.0;
}

static int update_main_surface(void)
{
	if (pres_i.surface_inited)
		destroy_main_surface();
	pres_i.surface_inited = true;

	SDL_GetWindowSize(pres_i.window, &pres.width, &pres.height);
	if (!pres.fullscreen) {
		pres_i.windowed_bounds.w = pres.width;
		pres_i.windowed_bounds.h = pres.height;
		SDL_GetWindowPosition(pres_i.window,
				&pres_i.windowed_bounds.x,
				&pres_i.windowed_bounds.y);
	}

	glViewport(0, 0, pres.width, pres.height);

	return 0;
}

static GLuint load_shader(GLenum type, const char *path)
{
	GLuint shader = glCreateShader(type);
	if (!shader)
		return 0;

	char *source = read_file_string(path);
	if (!source) {
		glDeleteShader(shader);
		return 0;
	}

	glShaderSource(shader, 1, (const GLchar **) &source, NULL);
	glCompileShaderIncludeARB(shader,
			pres_i.shader_libs.size,
			pres_i.shader_libs.a,
			pres_i.shader_lib_lengths.a);

	free(source);

	GLint compiled = GL_FALSE;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE) {
		GLint len = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
		if (!len) {
			glDeleteShader(shader);
			ERRF("Unknown GLSL error for file '%s'\n", path);
			return 0;
		}

		char buf[len + 1];
		glGetShaderInfoLog(shader, len + 1, &len, buf);
		glDeleteShader(shader);
		ERRF("GLSL error for file '%s': %s\n", path, buf);
		return 0;
	}

	return shader;
}


static int shader_program(enum shader_ix ix,
                          const char *vert_path,
                          const char *geom_path,
                          const char *frag_path)
{
	bool err = false;
	GLuint vert = 0;
	GLuint geom = 0;
	GLuint frag = 0;
	GLuint program = glCreateProgram();
	if (!program)
		return PRES_GLERR;

	if (vert_path) {
		char *path = program_file(vert_path);
		vert = load_shader(GL_VERTEX_SHADER, path);
		free(path);
		if (!vert) {
			err = true;
			goto exit;
		}

		glAttachShader(program, vert);
	}

	if (geom_path) {
		char *path = program_file(geom_path);
		geom = load_shader(GL_GEOMETRY_SHADER, path);
		free(path);
		if (!geom) {
			err = true;
			goto exit;
		}

		glAttachShader(program, geom);
	}

	if (frag_path) {
		char *path = program_file(frag_path);
		frag = load_shader(GL_FRAGMENT_SHADER, path);
		free(path);
		if (!frag) {
			err = true;
			goto exit;
		}

		glAttachShader(program, frag);
	}

	glLinkProgram(program);
	GLint linked = GL_FALSE;
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if (linked == GL_FALSE) {
		GLint len = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
		if (!len) {
			err = true;
			ERRPUT("Unknown GLSL link error\n");
			goto exit;
		}

		char buf[len + 1];
		glGetProgramInfoLog(program, len + 1, &len, buf);
		ERRF("GLSL link error: %s\n", buf);
		err = true;
		goto exit;
	}

exit:
	if (vert) glDetachShader(program, vert);
	if (geom) glDetachShader(program, geom);
	if (frag) glDetachShader(program, frag);

	if (err) {
		glDeleteProgram(program);
		return PRES_GLERR;
	}

	struct shader *s = &pres_i.shaders[ix];
	s->id = program;
	s->gu_mvp = glGetUniformLocation(program, "gu_mvp");
	s->gu_time = glGetUniformLocation(program, "gu_time");
	return PRES_SUCCESS;
}

void delete_shader_programs(void)
{
	for (size_t i = 0; i < ARRSIZEOF(pres_i.shaders); i++) {
		if (pres_i.shaders[i].id)
			glDeleteProgram(pres_i.shaders[i].id);
	}
}

void shader_lib(const char *name, const char *relpath)
{
	char *path = program_file(relpath);
	char *source = read_file_string(path);
	REQUIRE(source);
	free(path);

	int namelen = strlen(name);
	glNamedStringARB(GL_SHADER_INCLUDE_ARB, namelen, name,
			strlen(source), source);
	ARRAY_ADD(&pres_i.shader_libs, name);
	ARRAY_ADD(&pres_i.shader_lib_lengths, namelen);
	free(source);
}

void delete_shader_libs(void)
{
	for (size_t i = 0; i < pres_i.shader_libs.size; i++) {
		const char *name = pres_i.shader_libs.a[i];
		int length = pres_i.shader_lib_lengths.a[i];
		glDeleteNamedStringARB(length, name);
	}
}

int pres_init(void)
{
	int err;
	assert(pres_i.init_stage == 0);

	// Window and OpenGL context

	pres_i.window = SDL_CreateWindow("cats",
			SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
			800, 600,
			SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL |
			SDL_WINDOW_SHOWN);
	if (!pres_i.window) {
		print_sdl_error("SDL_Window");
		pres_deinit();
		return PRES_SDLERR;
	}
	pres_i.init_stage++;

	pres_i.context = SDL_GL_CreateContext(pres_i.window);
	if (!pres_i.context) {
		print_sdl_error("SDL_GL_CreateContext");
		pres_deinit();
		return PRES_SDLERR;
	}
	pres_i.init_stage++;

	SDL_ShowCursor(SDL_FALSE);

	// OpenGL configuration
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
	//SDL_GL_SetSwapInterval(0); // TODO: VSync - causes flickers when on

	glEnable(GL_FRAMEBUFFER_SRGB);

	glShadeModel(GL_SMOOTH);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//glEnable(GL_MULTISAMPLE);
	//glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	//glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
	//glEnable(GL_LINE_SMOOTH);
	//glEnable(GL_POLYGON_SMOOTH);

	glClearColor(1, 1, 1, 1);

	GLenum glew_err = glewInit();
	if (glew_err != GLEW_OK && glew_err != GLEW_ERROR_NO_GLX_DISPLAY) {
		ERRF("glewInit failed %u: %s\n", glew_err, glewGetErrorString(glew_err));
		pres_deinit();
		return PRES_GLERR;
	}

	// Shader libs
	pres_i.init_stage++;
	memset(&pres_i.shader_libs, 0, sizeof(pres_i.shader_libs));
	memset(&pres_i.shader_lib_lengths, 0, sizeof(pres_i.shader_lib_lengths));
	shader_lib("/vecs.glsl", "glsl/lib/vecs.glsl");

	// Shaders
	pres_i.init_stage++;
	memset(&pres_i.shaders, 0, sizeof(pres_i.shaders));
	err = shader_program(SHD_RECT,
			"glsl/rect.vert.glsl",
			NULL,
			"glsl/rect.frag.glsl");
	if (err) return err;

	// Arrays and buffers
	glGenVertexArrays(VAO_COUNT, pres_i.arrays);
	pres_i.init_stage++;
	glGenBuffers(VBO_COUNT, pres_i.buffers);
	pres_i.init_stage++;

	// Rectangles
	memset(&pres_i.rects, 0, sizeof(pres_i.rects));
	glBindVertexArray(pres_i.arrays[VAO_RECTS]);
	{
		glBindVertexBuffer(0, pres_i.buffers[VBO_RECTS_INSTS], 0,
				sizeof(struct bo_rect));
		{
			glVertexBindingDivisor(0, 1);

			glEnableVertexAttribArray(0); // pos
			glVertexAttribFormat(0, 2, GL_FLOAT, GL_FALSE,
					offsetof(struct bo_rect, pos));
			glVertexAttribBinding(0, 0);

			glEnableVertexAttribArray(1); // dims
			glVertexAttribFormat(1, 2, GL_FLOAT, GL_FALSE,
					offsetof(struct bo_rect, dims));
			glVertexAttribBinding(1, 0);

			glEnableVertexAttribArray(2); // rot
			glVertexAttribFormat(2, 1, GL_FLOAT, GL_FALSE,
					offsetof(struct bo_rect, rot));
			glVertexAttribBinding(2, 0);

			glEnableVertexAttribArray(3); // col
			glVertexAttribFormat(3, 4, GL_FLOAT, GL_FALSE,
					offsetof(struct bo_rect, col));
			glVertexAttribBinding(3, 0);
		}

		glBindVertexBuffer(1, pres_i.buffers[VBO_RECTS_VERTS], 0, sizeof(v2f));
		{
			glVertexBindingDivisor(1, 0);

			glEnableVertexAttribArray(4); // vertex
			glVertexAttribFormat(4, 2, GL_FLOAT, GL_FALSE, 0);
			glVertexAttribBinding(4, 1);
		}

		glBindBuffer(GL_ARRAY_BUFFER, pres_i.buffers[VBO_RECTS_VERTS]);
		glBufferData(GL_ARRAY_BUFFER,
				sizeof(rect_vertices), rect_vertices, GL_STATIC_DRAW);
	}

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Surface
	err = update_main_surface();
	if (err) {
		if (err == PRES_SDLERR)
			print_sdl_error("update_main_surface");
		else
			ERRPUT("update_main_surface error\n");
		pres_deinit();
		return err;
	}
	pres_i.init_stage++;

	return PRES_SUCCESS;
}

void pres_deinit(void)
{
	// all fallthroughs intentional
	switch (pres_i.init_stage) {
	case 7: destroy_main_surface();
	case 6: glDeleteVertexArrays(VAO_COUNT, pres_i.arrays);
	case 5: glDeleteBuffers(VBO_COUNT, pres_i.buffers);
	case 4: delete_shader_programs();
	case 3: delete_shader_libs();
	case 2: SDL_GL_DeleteContext(pres_i.context);
	case 1: SDL_DestroyWindow(pres_i.window);

	default:
		ARRAY_DEINIT(&pres_i.shader_libs);
		ARRAY_DEINIT(&pres_i.shader_lib_lengths);
		ARRAY_DEINIT(&pres_i.rects);

		pres = (struct pres){};
		pres_i = (struct pres_i){};
	}
}

static void toggle_fullscreen(void)
{
	pres.fullscreen ^= true;
	if (pres.fullscreen) {
		SDL_Rect rect = {};
		int display = SDL_GetWindowDisplayIndex(pres_i.window);
		SDL_GetDisplayBounds(display, &rect);

		SDL_SetWindowFullscreen(pres_i.window, SDL_WINDOW_FULLSCREEN);
		SDL_SetWindowSize(pres_i.window, rect.w, rect.h);
	} else {
		SDL_SetWindowFullscreen(pres_i.window, 0);
		SDL_SetWindowSize(pres_i.window,
				pres_i.windowed_bounds.w,
				pres_i.windowed_bounds.h);
		SDL_SetWindowPosition(pres_i.window,
				pres_i.windowed_bounds.x,
				pres_i.windowed_bounds.y);
	}
	update_main_surface();
}

static void update_global_uniforms(void)
{
	GLfloat mvp[16];
	v2f ppos = game.camera.pos;
	v2f hdim = {
		.x = pres.width / 2,
		.y = pres.height / 2,
	};
	ortho(mvp,
			ppos.x - hdim.x,
			ppos.x + hdim.x,
			ppos.y - hdim.y,
			ppos.y + hdim.y,
			-1, 1);
	float time = game.time * 0.001f; // TODO

	for (size_t i = 0; i < ARRSIZEOF(pres_i.shaders); i++) {
		struct shader *s = &pres_i.shaders[i];
		glUseProgram(s->id);
		if (s->gu_mvp >= 0)
			glUniformMatrix4fv(s->gu_mvp, 1, GL_TRUE, mvp);
		if (s->gu_time >= 0)
			glUniform1f(s->gu_time, time);
	}
}

static bool cat_generate_func(void *item, void *baton)
{
	struct cat *c = item;
	ARRAY_ADD(&pres_i.rects, ((struct bo_rect){
		.pos = c->phys->pos,
		.dims = { { 20, 20 } },
		.rot = c->angle,
		.col = {
			.r = c->color.r,
			.g = c->color.g,
			.b = c->color.b,
			.a = 0.5,
		},
	}));
	return true;
}

static bool particle_generate_func(void *item, void *baton)
{
	struct particle *p = item;
	switch (p->type) {
	case PARTICLE_TYPE_PAW:
		ARRAY_ADD(&pres_i.rects, ((struct bo_rect){
			.pos = p->pos,
			.dims = { { 5, 5 } },
			.col = {
				.a = (1.0f - ((float)p->age / (float)p->age_max)) * .5f
			}
		}));
		break;

	case PARTICLE_TYPE_NULL:
		break; // NOP
	}
	return true;
}

static void render_rects(void)
{
	glUseProgram(pres_i.shaders[SHD_RECT].id);
	glBindBuffer(GL_ARRAY_BUFFER, pres_i.buffers[VBO_RECTS_INSTS]);
	glBufferData(GL_ARRAY_BUFFER,
			ARRAY_DATA_SIZE(&pres_i.rects),
			pres_i.rects.a, GL_DYNAMIC_DRAW);

	glBindVertexArray(pres_i.arrays[VAO_RECTS]);
	glDrawElementsInstanced(GL_TRIANGLES,
			ARRSIZEOF(rect_indices), GL_UNSIGNED_BYTE, rect_indices,
			pres_i.rects.size);
}

void pres_update_surfaces(void)
{
	if (input.e.resized)
		update_main_surface();
	if (input.e.fullscreen)
		toggle_fullscreen();
}

void pres_update_render(uint64_t d_usec)
{
	// Prepare
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	update_global_uniforms();
	ARRAY_CLEAR(&pres_i.rects);

	// Generate buffer objects
	mempool_apply(game.cats, cat_generate_func, NULL);
	ARRAY_ADD(&pres_i.rects, ((struct bo_rect){
		.pos = game.world_mouse,
		.dims = { .x = 5, .y = 5 },
		.rot = M_PI_4,
		.col = {
			.r = 1,
			.a = 1,
		}
	}));
	mempool_apply(game.particles, particle_generate_func, NULL);

	// Render
	render_rects();

	// Cleanup and draw
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

	SDL_GL_SwapWindow(pres_i.window);

	// Window title
	char title[256];
	snprintf(title, sizeof(title), "cats | %3.0f FPS | delta: %.4f s | %f %f",
			roundf(1000000.0f / d_usec), d_usec * 0.000001f,
			game.player_cat->phys->pos.x, game.player_cat->phys->pos.y);
	SDL_SetWindowTitle(pres_i.window, title);
}
