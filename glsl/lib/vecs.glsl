#define PI 3.14159265358979323846

vec2 rotate(vec2 u, float angle)
{
    float l = length(u);
    angle += atan(u.y, u.x);

    return vec2(cos(angle), sin(angle)) * l;
}
