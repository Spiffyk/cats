#version 440 core
#extension GL_ARB_shading_language_include : require

#include "/vecs.glsl"

uniform mat4 gu_mvp;

layout(location = 0) in vec2 pos;
layout(location = 1) in vec2 dims;
layout(location = 2) in float rot;
layout(location = 3) in vec4 col;
layout(location = 4) in vec2 vertex;

out vec4 vert_col;

void main()
{
	vec2 v = rotate(vertex * dims, rot);
	gl_Position = gu_mvp * vec4(v + pos, 0, 1);
	vert_col = col;
}
