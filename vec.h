#pragma once

#include <math.h>

// Float ///////////////////////////////////////////////////////////////////////

/** Performs a linear interpolation between `a` and `b` by factor `t`. */
static inline float float_mix(float a, float b, float t)
{
	return a + (b - a) * t;
}


// Vector 2D ///////////////////////////////////////////////////////////////////

/** A 2D vector. */
typedef union {
	struct {
		float x;
		float y;
	};
	float f[2];
} v2f;

/** Adds `u` and `v` without modifying them; returns the result. */
static inline v2f v2f_add(v2f u, v2f v)
{
	return (v2f){
		.x = u.x + v.x,
		.y = u.y + v.y,
	};
}

/** Adds `src` to `dst`. Writes the result to `dst`. */
static inline void v2f_addto(v2f *dst, v2f src)
{
	dst->x += src.x;
	dst->y += src.y;
}

/** Subtracts `u` and `v` without modifying them; returns the result. */
static inline v2f v2f_sub(v2f u, v2f v)
{
	return (v2f){
		.x = u.x - v.x,
		.y = u.y - v.y
	};
}

/** Multiplies `u` by `factor` without modifying `u`; returns the result. */
static inline v2f v2f_scaled(v2f u, float factor)
{
	return (v2f){
		.x = u.x * factor,
		.y = u.y * factor
	};
}

/** Divides `u` by `factor` without modifying `u`; returns the result. */
static inline v2f v2f_div(v2f u, float factor)
{
	return (v2f){
		.x = u.x / factor,
		.y = u.y / factor
	};
}

/** Gets the length of `u`, squared. */
static inline float v2f_sqlength(v2f u)
{
	return u.x * u.x + u.y * u.y;
}

/** Gets the length of `u`. */
static inline float v2f_length(v2f u)
{
	return sqrtf(v2f_sqlength(u));
}

/** Gets `u` normalized to length of 1. */
static inline v2f v2f_normalize(v2f u)
{
	return v2f_div(u, v2f_length(u));
}

/** Gets a vector with the same direction as `u`, but with `length`. */
static inline v2f v2f_withlength(v2f u, float length)
{
	return v2f_scaled(v2f_normalize(u), length);
}

/** Performs a linear interpolation between `u` and `v` by factor `t`. */
static inline v2f v2f_mix(v2f u, v2f v, float t)
{
	return (v2f){
		.x = float_mix(u.x, v.x, t),
		.y = float_mix(u.y, v.y, t)
	};
}

static inline v2f v2f_clamp(v2f u, float maxlen)
{
	if (v2f_sqlength(u) <= (maxlen * maxlen))
		return u;

	return v2f_withlength(u, maxlen);
}

static inline float v2f_angle(v2f u)
{
	return atan2f(u.y, u.x);
}

static inline v2f v2f_rotate(v2f u, float angle)
{
	float l = v2f_length(u);
	angle += v2f_angle(u);
	return v2f_scaled((v2f) { .x = cosf(angle), .y = sinf(angle) }, l);
}

static inline v2f v2f_alen(float angle, float length)
{
	return (v2f){
		.x = cosf(angle) * length,
		.y = sinf(angle) * length
	};
}

static inline v2f v2f_reflect(v2f u, v2f normal)
{
	float l = v2f_length(u);
	float u_angle = v2f_angle(u);
	float n_angle = v2f_angle(normal);
	float r_angle = 2 * n_angle + M_PI - u_angle;
	return v2f_alen(r_angle, l);
}


// Vector 3D ///////////////////////////////////////////////////////////////////

/** A 3D vector or an RGB color. */
typedef union {
	struct {
		union { float x; float r; };
		union { float y; float g; };
		union { float z; float b; };
	};
	float f[3];
} v3f;

/** Adds `u` and `v` without modifying them; returns the result. */
static inline v3f v3f_add(v3f u, v3f v)
{
	return (v3f){
		.x = u.x + v.x,
		.y = u.y + v.y,
		.z = u.z + v.z,
	};
}

/** Adds `src` to `dst`. Writes the result to `dst`. */
static inline void v3f_addto(v3f *dst, v3f src)
{
	dst->x += src.x;
	dst->y += src.y;
	dst->z += src.z;
}

/** Performs a linear interpolation between `u` and `v` by factor `t`. */
static inline v3f v3f_mix(v3f u, v3f v, float t)
{
	return (v3f){
		.x = float_mix(u.x, v.x, t),
		.y = float_mix(u.y, v.y, t),
		.z = float_mix(u.z, v.z, t)
	};
}


// Vector 4D ///////////////////////////////////////////////////////////////////

typedef union {
	struct {
		union { float x; float r; };
		union { float y; float g; };
		union { float z; float b; };
		union { float w; float a; };
	};
	float f[4];
} v4f;

/** Adds `u` and `v` without modifying them; returns the result. */
static inline v4f v4f_add(v4f u, v4f v)
{
	return (v4f){
		.x = u.x + v.x,
		.y = u.y + v.y,
		.z = u.z + v.z,
		.w = u.w + v.w,
	};
}

/** Adds `src` to `dst`. Writes the result to `dst`. */
static inline void v4f_addto(v4f *dst, v4f src)
{
	dst->x += src.x;
	dst->y += src.y;
	dst->z += src.z;
	dst->w += src.w;
}

/** Performs a linear interpolation between `u` and `v` by factor `t`. */
static inline v4f v4f_mix(v4f u, v4f v, float t)
{
	return (v4f){
		.x = float_mix(u.x, v.x, t),
		.y = float_mix(u.y, v.y, t),
		.z = float_mix(u.z, v.z, t),
		.w = float_mix(u.w, v.w, t),
	};
}
