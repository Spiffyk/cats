#ifndef __GNUC__
#error This program uses GNU extensions and needs a compiler that implements them.
#endif

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "SDL2/SDL.h"

#include "game.h"
#include "input.h"
#include "pres.h"
#include "utils.h"

#define FPS_LIMIT 120
#define DELTA_MIN (1'000'000 / FPS_LIMIT)
#define LOG_LENGTH_NSEC 1'000'000

static inline uint64_t calc_delta(uint64_t last_time, uint64_t *curr_time)
{
	*curr_time = clock_usec(CLOCK_MONOTONIC);
	return *curr_time - last_time;
}

int main(void)
{
	int err;
	int ret = EXIT_SUCCESS;

	// Initializations
	err = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_JOYSTICK);
	if (err) {
		print_sdl_error("SDL_Init");
		ret = EXIT_FAILURE;
		goto exit;
	}

	err = input_init();
	if (err) {
		ERRPUT("input_init error\n");
		ret = EXIT_FAILURE;
		goto exit_sdl;
	}

	err = game_init();
	if (err) {
		ERRPUT("game_init error\n");
		ret = EXIT_FAILURE;
		goto exit_input;
	}

	err = pres_init();
	if (err) {
		ERRPUT("pres_init error\n");
		ret = EXIT_FAILURE;
		goto exit_game;
	}

	// Main loop
	uint64_t last_time = clock_usec(CLOCK_MONOTONIC);
	while (!input.e.quit) {
		uint64_t curr_time;
		uint64_t d_usec;
		while ((d_usec = calc_delta(last_time, &curr_time)) < DELTA_MIN)
			usleep(DELTA_MIN - d_usec);

		input_update_events();
		pres_update_surfaces();
		game_update(d_usec);
		pres_update_render(d_usec);

		last_time = curr_time;
	}

	// Cleanups
	pres_deinit();
exit_game:
	game_deinit();
exit_input:
	input_deinit();
exit_sdl:
	SDL_Quit();
exit:
	return ret;
}
