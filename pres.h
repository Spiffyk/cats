#pragma once

#include <stdbool.h>
#include <stdint.h>

enum {
	PRES_SUCCESS = 0,
	PRES_SDLERR,
	PRES_GLERR
};

struct pres {
	int width;
	int height;
	bool fullscreen;
};

extern struct pres pres;

[[nodiscard]]
int pres_init(void);
void pres_deinit(void);

void pres_update_surfaces(void);
void pres_update_render(uint64_t d_usec);
