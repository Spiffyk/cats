#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "vec.h"

struct input_events {
	bool resized : 1;
	bool quit : 1;
	bool fullscreen : 1;
};

struct input {
	int mouse_x;
	int mouse_y;
	int16_t left_joy_x;
	int16_t left_joy_y;
	bool mouse_left_down : 1;
	bool has_joystick : 1;
	struct input_events e;
};

extern struct input input;

[[nodiscard]]
int input_init(void);
void input_deinit(void);

void input_update_events(void);

static inline v2f input_mouse_v2f(void)
{
	return (v2f){
		.x = input.mouse_x,
		.y = input.mouse_y
	};
}

static inline v2f input_left_joy_v2f(void)
{
	return (v2f){
		.x = (float)input.left_joy_x / (float)INT16_MAX,
		.y = (float)input.left_joy_y / (float)INT16_MAX,
	};
}
