#include <errno.h>
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#include "input.h"
#include "pres.h"

#include "game.h"

/** Information about memory planned to be freed at the end of the current
 * frame's processing. */
struct destruction_plan {
	struct mempool *mempool;
	void *mem;
};


// Entities ////////////////////////////////////////////////////////////////////

/** An entry in `struct entity_map`. */
struct entity_reg {
	entity_id_t id;
	struct entity *entity;
};

#define ENTITY_MAP_LENGTH 2039 /**< Hash modulo divisor. */
#define ENTITY_MAP_WIDTH 8     /**< Hash collision slack. */

/** A chunk of a hashtable containing pointers to entities. */
struct entity_map {
	/** Number of entities in this chunk. */
	size_t num_entities;
	/** Hashtable. */
	struct entity_reg regs[ENTITY_MAP_LENGTH][ENTITY_MAP_WIDTH];
	/** The next chunk. */
	struct entity_map *next;
};

/** State of the game internal to the module. */
struct game_i {
	uint64_t log_time;
	lcg_t rand;
	ARRAY(struct destruction_plan) to_destroy;

	entity_id_t entity_id_seq;
	struct entity_map *entities;
};

struct game game;
static struct game_i game_i;

/** Puts the specified entity into the entity registry. */
static void register_entity(struct entity *entity)
{
	assert(!entity->base.id);
	entity->base.id = ++game_i.entity_id_seq;
	assert(entity->base.id);

	if (!game_i.entities) {
		game_i.entities = calloc(1, sizeof(struct entity_map));
		REQUIRE(game_i.entities);
	}
	struct entity_map *m = game_i.entities;

	size_t index = entity->base.id % ENTITY_MAP_LENGTH;

	for (;;) {
		struct entity_reg *slots = m->regs[index];
		for (size_t i = 0; i < ENTITY_MAP_WIDTH; i++) {
			if (slots[i].id == entity->base.id) {
				slots[i].id = entity->base.id;
				return;
			}

			if (!slots[i].id) {
				m->num_entities++;
				slots[i] = (struct entity_reg){
					.id = entity->base.id,
					.entity = entity
				};
				return;
			}
		}

		if (m->next)
			m = m->next;
		else {
			m = m->next = calloc(1, sizeof(struct entity_map));
			REQUIRE(m);
		}
	}
}

/** Removes the specified entity from the entity registry. */
static int unregister_entity(struct entity *entity)
{
	if (!entity->base.id)
		return EEXIST;

	size_t index = entity->base.id % ENTITY_MAP_LENGTH;
	struct entity_map **m = &game_i.entities;

	for (;;) {
		struct entity_reg *slots = (*m)->regs[index];
		for (size_t i = 0; i < ENTITY_MAP_WIDTH; i++) {
			if (slots[i].id == entity->base.id) {
				entity->base.id = 0;
				(*m)->num_entities--;

				if (!(*m)->num_entities) {
					struct entity_map *next = (*m)->next;
					free(*m);
					*m = next;
					return 0;
				}

				slots[i].id = 0;
				if (i < ENTITY_MAP_WIDTH - 1) {
					size_t s = sizeof(*slots) * (ENTITY_MAP_WIDTH - i - 1);
					memmove(&slots[i], &slots[i + 1], s);
				}
				return 0;
			}

			if (!slots[i].id)
				return EEXIST;
		}

		m = &(*m)->next;
		if (!*m)
			return EEXIST;
	}
}

/** Finds an entity in the entity registry. */
[[nodiscard]]
static struct entity *find_entity(entity_id_t id)
{
	struct entity_map *m = game_i.entities;
	size_t index = id % ENTITY_MAP_LENGTH;

	for (;;) {
		if (!m)
			return NULL;

		struct entity_reg *slots = m->regs[index];
		for (size_t i = 0; i < ENTITY_MAP_WIDTH; i++) {
			if (slots[i].id == id) {
				assert(slots[i].id == slots[i].entity->base.id);
				return slots[i].entity;
			}

			if (!slots[i].id)
				return NULL;
		}

		m = m->next;
	}
}

/** Destroys the specified entity. */
static inline void destroy_generic_entity_func(void *item)
{
	unregister_entity(item);
}


// Cat /////////////////////////////////////////////////////////////////////////

#define CAT_MAX_CONTROL_FORCE 20000.f
#define CAT_MAX_CONTROL_FORCE_MOUSE_DISTANCE 200.f
#define CAT_JOY_INNER_DEADZONE 0.1
#define CAT_JOY_OUTER_DEADZONE 0.9
#define CAT_JOY_DEADZONE_DIVISOR (CAT_JOY_OUTER_DEADZONE - CAT_JOY_INNER_DEADZONE)
#define CAT_PAW_ANGLE_PARAM 0.45
#define CAT_PAW_SPAWN_SEC 20
#define CAT_PAW_AGE_MAX_MSEC 1000
#define CAT_PAW_RADIUS 30

const float cat_paw_angles[] = {
	           CAT_PAW_ANGLE_PARAM,
	M_PI     + CAT_PAW_ANGLE_PARAM,
	2 * M_PI - CAT_PAW_ANGLE_PARAM,
	M_PI     - CAT_PAW_ANGLE_PARAM
};

static struct cat *cat_create(float x, float y)
{
	struct cat *c = mempool_alloc(game.cats);
	c->base.type = ENTITY_TYPE_CAT;
	c->color = (v3f){
		.r = 1,
		.g = 0.5,
		.b = 0,
	};
	c->angle = lcg_next_f(&game_i.rand) * M_PI * 2;
	c->time_to_paw = lcg_next_f(&game_i.rand) * CAT_PAW_SPAWN_SEC;

	c->ppos = (v2f){ .x = x, .y = y };
	c->phys = mempool_alloc(game.phys);
	*c->phys = (struct phys){
		.owner = (struct entity *)c,
		.pos = {
			.x = x,
			.y = y
		},
		.mass = 2.f,
		.drag = .2f,
		.col = {
			.type = COLLIDER_CIRCLE
		}
	};

	register_entity((struct entity *)c);
	return c;
}

static void cat_apply_control(struct cat *cat, const v2f *input)
{
	v2f dir = v2f_clamp(*input, 1.0f);
	cat->angle = v2f_angle(dir);
	v2f_addto(&cat->phys->force, v2f_scaled(dir, CAT_MAX_CONTROL_FORCE));
}


// Particles ///////////////////////////////////////////////////////////////////

static struct particle *particle_create(enum particle_type type, v2f pos,
                                        v2f vel, uint64_t age_max)
{
	struct particle *p = mempool_alloc(game.particles);
	*p = (struct particle){
		.type = type,
		.pos = pos,
		.vel = vel,
		.age_max = age_max,
	};
	return p;
}


// Game ////////////////////////////////////////////////////////////////////////

int game_init(void)
{
	memset(&game, 0, sizeof(game));
	game.phys = mempool_create(1024, sizeof(struct phys), NULL);
	game.cats = mempool_create(256, sizeof(struct cat), destroy_generic_entity_func);
	game.particles = mempool_create(4096, sizeof(struct particle), NULL);

	memset(&game_i, 0, sizeof(game_i));
	game_i.rand = lcg_seed_time();

	game.player_cat = cat_create(100, 100);
	game.player_cat->color = (v3f) { .f = { 1, 0, 0 } };

	for (unsigned int i = 0; i < 200; i++) {
		cat_create(lcg_next_f(&game_i.rand) * 2000, lcg_next_f(&game_i.rand) * 2000);
	}

	return 0;
}

void game_deinit(void)
{
	mempool_destroy(game.phys);
	mempool_destroy(game.cats);
}

/** Plans the specified `mem` from `mempool` for destruction at the end
 * of the currently processed frame. */
static inline void plan_destroy(struct mempool *mempool, void *mem)
{
	ARRAY_ADD(&game_i.to_destroy, ((struct destruction_plan){
		.mempool = mempool,
		.mem = mem
	}));
}

struct update_ctx {
	uint64_t d_mil;
	float d_sec;
};

static bool phys_pos_mpupdate_func(void *item, void *baton)
{
	struct phys *phys = item;
	const struct update_ctx *ctx = baton;

	v2f_addto(&phys->force, v2f_scaled(phys->vel, -(.5f * v2f_length(phys->vel) * phys->drag))); // Apply drag
	v2f acc = v2f_scaled(phys->force, ctx->d_sec / phys->mass);
	acc = v2f_clamp(acc, 100.f);
	v2f_addto(&phys->vel, acc); // force to acceleration to velocity
	v2f_addto(&phys->pos, v2f_scaled(phys->vel, ctx->d_sec)); // velocity to position

	// force is an impulse applied once per frame; zero out for the next one
	phys->force = (v2f){0};

	return true;
}

static bool is_colliding(struct phys *a, struct phys *b)
{
	//if (a->col.type > b->col.type) {
	//	struct phys *h = a;
	//	a = b;
	//	b = h;
	//}

	if (a->col.type == COLLIDER_NULL)
		return false;

	if (a->col.type == COLLIDER_CIRCLE) {
		if (b->col.type == COLLIDER_CIRCLE) {
			float rad = a->col.circle.radius + b->col.circle.radius;
			v2f sub = v2f_sub(a->pos, b->pos);
			return v2f_sqlength(sub) < (rad * rad);
		}
	}

	return false;
}

static void update_camera(void)
{
	game.camera.pos = game.player_cat->phys->pos; // TODO: nicer camera

	v2f mouse_pos = v2f_sub(input_mouse_v2f(), (v2f){
		.x = pres.width / 2,
		.y = pres.height / 2,
	});
	game.world_mouse = v2f_add(mouse_pos, game.camera.pos);
}

static void player_cat_handle_input(void)
{
	struct cat *p = game.player_cat;
	if (!p)
		return;

	v2f dir = {0};
	bool had_input = false;
	if (input.mouse_left_down) {
		v2f dir_dist = v2f_clamp(
				v2f_sub(game.world_mouse, p->phys->pos),
				CAT_MAX_CONTROL_FORCE_MOUSE_DISTANCE);
		v2f_addto(&dir, v2f_div(dir_dist, CAT_MAX_CONTROL_FORCE_MOUSE_DISTANCE));
		had_input = true;
	}

	if (input.has_joystick) {
		v2f ljoy = input_left_joy_v2f();
		float ljoylen = v2f_length(ljoy);
		if (ljoylen > CAT_JOY_INNER_DEADZONE) {
			v2f joy_input = v2f_scaled(ljoy, (ljoylen - CAT_JOY_INNER_DEADZONE) / CAT_JOY_DEADZONE_DIVISOR);
			v2f_addto(&dir, joy_input);
			had_input = true;
		}
	}

	if (had_input)
		cat_apply_control(p, &dir);
}

static bool phys_col_mpcompare_func(void *item, void *baton)
{
	struct phys *other = item;
	struct phys *self = baton;

	if (self->col.num_cols >= MAX_COLLISIONS)
		return false;

	if (self == other)
		return true;

	if (is_colliding(self, other))
		self->col.cols[self->col.num_cols++] = other->owner->base.id;

	return true;
}

static bool phys_col_mpupdate_func(void *item, void *baton)
{
	/* TODO: This currently tests against ALL other entities with colliders
	 * (n^2 complexity). I want to do some kind of bucketing at some point. */
	struct phys *self = item;
	self->col.num_cols = 0;
	mempool_apply(game.phys, phys_col_mpcompare_func, item);
	return true;
}

static bool cat_mpupdate_func(void *item, void *baton)
{
	struct cat *cat = item;
	const struct update_ctx *ctx = baton;

	float travel_dist = v2f_length(v2f_sub(cat->ppos, cat->phys->pos));

	cat->time_to_paw -= travel_dist;
	while (cat->time_to_paw < 0) {
		cat->time_to_paw += CAT_PAW_SPAWN_SEC;
		const v2f paw_vec = v2f_rotate(
				(v2f){ .x = CAT_PAW_RADIUS },
				cat_paw_angles[cat->paw] + cat->angle);
		const v2f paw_pos = v2f_add(cat->phys->pos, paw_vec);
		particle_create(PARTICLE_TYPE_PAW, paw_pos, (v2f){}, CAT_PAW_AGE_MAX_MSEC);
		cat->paw = (cat->paw + 1) % ARRSIZEOF(cat_paw_angles);
	}

	cat->ppos = cat->phys->pos;

	return true;
}

static bool particle_mpupdate_func(void *item, void *baton)
{
	struct particle *p = item;
	const struct update_ctx *ctx = baton;

	// TODO: Copied over from phys, but simplified for particles. May add drag later

//	v2f drag_force = v2f_scaled(p->vel, -(.5f * v2f_length(p->vel) * p->drag)); // Apply drag
//	v2f acc = v2f_scaled(drag_force, ctx->d_sec);
//	acc = v2f_clamp(acc, 100.f);
//	v2f_addto(&p->vel, acc); // force to acceleration to velocity
	v2f_addto(&p->pos, v2f_scaled(p->vel, ctx->d_sec)); // velocity to position

	p->age += ctx->d_mil;
	if (p->age >= p->age_max)
		plan_destroy(game.particles, p);

	return true;
}

void game_update(uint64_t d_usec)
{
	uint64_t d_mil = d_usec / 1000;
	game.time += d_mil;
	game_i.log_time += d_mil;
	float d_sec = d_usec * 0.000001f;

	update_camera();

	player_cat_handle_input();
	struct update_ctx ctx = {
		.d_mil = d_mil,
		.d_sec = d_sec
	};
	// TODO: update entities here
	mempool_apply(game.phys, phys_pos_mpupdate_func, &ctx);
	mempool_apply(game.phys, phys_col_mpupdate_func, NULL);
	mempool_apply(game.cats, cat_mpupdate_func, &ctx);

	mempool_apply(game.particles, particle_mpupdate_func, &ctx);

	for (size_t i = 0; i < game_i.to_destroy.size; i++) {
		struct destruction_plan *plan = &game_i.to_destroy.a[i];
		mempool_free(plan->mempool, plan->mem);
	}
	ARRAY_CLEAR(&game_i.to_destroy);
}
