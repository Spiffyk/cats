#include <assert.h>

#include "SDL2/SDL.h"

#include "utils.h"

#include "input.h"

struct input_i {
	int init_stage;
	SDL_Joystick *joystick;
};

struct input input;
struct input_i input_i;

int input_init(void)
{
	assert(input_i.init_stage == 0);
	memset(&input, 0, sizeof(input));
	memset(&input_i, 0, sizeof(input_i));

	input_i.init_stage++;

	if (SDL_NumJoysticks() > 0) {
		input_i.joystick = SDL_JoystickOpen(0);
		if (input_i.joystick) {
			input.has_joystick = true;
		} else {
			ERRPUT("Could not open joystick...");
		}
	}

	return 0;
}

void input_deinit(void)
{
	if (input_i.joystick)
		SDL_JoystickClose(input_i.joystick);

	input_i.init_stage = 0;
}

void input_update_events(void)
{
	memset(&input.e, 0, sizeof(input.e));

	SDL_Event event;
	while (SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_WINDOWEVENT:
			switch (event.window.event) {
				case SDL_WINDOWEVENT_RESIZED:
					input.e.resized = true;
					break;
			}
			break;
		case SDL_MOUSEMOTION:
			input.mouse_x = event.motion.x;
			input.mouse_y = event.motion.y;
			break;
		case SDL_MOUSEBUTTONDOWN:
			if (event.button.button == SDL_BUTTON_LEFT)
				input.mouse_left_down = true;
			break;
		case SDL_MOUSEBUTTONUP:
			if (event.button.button == SDL_BUTTON_LEFT)
				input.mouse_left_down = false;
			break;
		case SDL_KEYUP:
			if (event.key.keysym.sym == SDLK_q)
				input.e.quit = true;
			if (event.key.keysym.sym == SDLK_F11)
				input.e.fullscreen = true;
			break;
		case SDL_QUIT:
			input.e.quit = true;
			break;
		}
	}

	input.left_joy_x = SDL_JoystickGetAxis(input_i.joystick, 0);
	input.left_joy_y = SDL_JoystickGetAxis(input_i.joystick, 1);
}
