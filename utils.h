#pragma once

#include <assert.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


// Logging /////////////////////////////////////////////////////////////////////

/** Prints a formatted message to `stderr`. */
#define ERRF(fmt, ...) fprintf(stderr, fmt, __VA_ARGS__)

#define ERRPUT(str) fputs((str), stderr)

/** Prints an SDL error message prepended with `title` to `stderr`. */
int print_sdl_error(const char *title);

/** A non-debug assertion - aborts the program if `assertion` is `false`. */
#define REQUIRE(assertion) do {\
	if (!(assertion)) {\
		ERRF("%s:%d: requirement failed: (%s)\n", \
				__FILE__, __LINE__, #assertion);\
		abort();\
	}\
} while (0)

/** Converts the specified boolean expression into a `true` or `false` string. */
#define BOOL_STR(boolv) ((boolv) ? "true" : "false")


// Allocating sprintf (GNU-like) ///////////////////////////////////////////////

/** Allocates a new formatted string, writing a pointer to it to `strp`.
 * Like GNU `asprintf()`. */
int xasprintf(char **strp, const char *fmt, ...);

/** Allocates a new formatted string, writing a pointer to it to `strp`.
 * Like GNU `asprintf()`. */
int vxasprintf(char **strp, const char *fmt, va_list ap);


// Dynamic arrays //////////////////////////////////////////////////////////////

/** The initial capacity of a dynamic array. */
#define ARRAY_INITIAL_CAPACITY 4

/** Defines a dynamic array type. The struct conforms to ZII - zeroing it
 * out initializes it properly. */
#define ARRAY(p_type) struct {\
	size_t capacity; /**< The current size of the array. */\
	size_t size; /**< The current number of items in the array. */\
	p_type *a; /**< The array. */\
}

/** Ensures that `p_arr` has enough capacity to accommodate at least `p_num`
 * elements. `p_arr` is a pointer to a dynamic array defined using
 * the `ARRAY(p_type)` macro. */
#define ARRAY_RESERVE(p_arr, p_num) do {\
	if ((p_arr)->capacity >= p_num)\
		break;\
	if (!(p_arr)->capacity)\
		(p_arr)->capacity = ARRAY_INITIAL_CAPACITY;\
	while ((p_arr)->capacity < p_num)\
		(p_arr)->capacity *= 2;\
	(p_arr)->a = realloc((p_arr)->a, (p_arr)->capacity * sizeof(*(p_arr)->a));\
} while (0)

/** Adds `p_val` to `p_arr`, making sure there is enough space. `p_arr` is
 * a pointer to a dynamic array defined using the `ARRAY(p_type)` macro. */
#define ARRAY_ADD(p_arr, p_val) do {\
	ARRAY_RESERVE((p_arr), (p_arr)->size + 1);\
	(p_arr)->a[(p_arr)->size++] = (p_val);\
} while (0)

/** Adds a new zeroed-out entry to `p_arr`, making sure there is enough space.
 * `p_arr` is a pointer to a dynamic array defined using the `ARRAY(p_type)`
 * macro. */
#define ARRAY_ADD_ZERO(p_arr) do {\
	ARRAY_RESERVE((p_arr), (p_arr)->size + 1);\
	memset(&(p_arr)->a[(p_arr)->size++], 0, sizeof(*(p_arr)->a));\
} while (0)

/** Gets the last entry of `p_arr`. */
#define ARRAY_TAIL(p_arr) \
({ \
	assert((p_arr)->size > 0); \
	(&(p_arr)->a[(p_arr)->size - 1]); \
})

/** Marks `p_arr` empty. */
#define ARRAY_CLEAR(p_arr) do {\
	(p_arr)->size = 0;\
} while (0)

/** Gets the size of the data in `p_arr` in bytes. */
#define ARRAY_DATA_SIZE(p_arr) (sizeof((p_arr)->a[0]) * (p_arr)->size)

/** Frees the array in `p_arr` (but not `p_arr` itself). `p_arr` is a pointer
 * to a dynamic array defined using the `ARRAY(p_type)` macro.*/
#define ARRAY_DEINIT(p_arr) free((p_arr)->a)


// Filesystem //////////////////////////////////////////////////////////////////

/** Gets the program executable path. */
const char *program_exe_path(void);

/** Gets the program directory path (parent of `program_exe_path()`). */
const char *program_dir_path(void);

/** Gets the path to a file relative to the program's executable path. Path is
 * heap-allocated and must be freed afterwards. */
[[nodiscard]]
char *program_file(const char *relpath);

/** Creates a null-terminated string from the contents of the specified file.
 * The file must not contain a NUL character, that is undefined behaviour.
 * Contents are heap-allocated and must be freed afterwards. */
[[nodiscard]]
char *read_file_string(const char *path);


// Time ////////////////////////////////////////////////////////////////////////

/** Gets the current time of the specified clock in milliseconds. */
uint64_t clock_millis(clockid_t clock);

/** Gets the current time of the specified clock in microseconds. */
uint64_t clock_usec(clockid_t clock);


// Random number generator /////////////////////////////////////////////////////

#define LCG_A 6364136223846793005ULL
#define LCG_C 1L

/** The state of a linear congruential generator (LCG) for pseudo-random number
 * generation. Implemented in this project because in some libraries, the number
 * of bits from rand() may actually be as low as 16, which would be insufficient
 * for the graph generator. Constants are taken from the MUSL library. */
typedef uint64_t lcg_t;

/** Creates a new LCG seeded with the specified value. */
static inline lcg_t lcg_seed(uint64_t seed)
{
	return seed - 1;
}

/** Creates a new LCG seeded with the current system time. */
static inline lcg_t lcg_seed_time(void)
{
	return clock_millis(CLOCK_REALTIME);
}

/** Generates the next random number in the LCG. */
static inline void _lcg_next(lcg_t *gen)
{
	*gen = LCG_A * (*gen) + LCG_C;
}

/** Gets the next random unsigned int in the LCG. */
static inline uint32_t lcg_next_u(lcg_t *gen)
{
	_lcg_next(gen);
	return (uint32_t) (*gen >> 32);
}

/** Gets the next random signed int in the LCG. */
static inline int32_t lcg_next_i(lcg_t *gen)
{
	_lcg_next(gen);
	return (int32_t) (*gen >> 32);
}

/** Gets the next random float in the range 0.0-1.0 in the LCG. */
static inline float lcg_next_f(lcg_t *gen)
{
	_lcg_next(gen);
	return (float) ((double) lcg_next_u(gen) / (double) UINT32_MAX);
}


// Strings /////////////////////////////////////////////////////////////////////

/** Compares two strings `a` and `b` with respective lengths `a_len` and
 * `b_len`.
 *
 * If `a_len` is less than `b_len`, returns a negative number; if `a_len` is
 * greater than `b_len`, returns a positive number. If lengths are the same,
 * returns the result of `strncmp()` on the strings. */
int sstrncmp(const char *a, size_t a_len, const char *b, size_t b_len);

/** Compares two strings `a` and `b`, where `b` is a compile-time constant
 * C-string and `a` is a string of length `a_len`. The rest is according to the
 * rules of `sstrncmp()`. */
#define SSTRNCMP_CONST(a, a_len, b) sstrncmp((a), (a_len), (b), strlen(b))

/** Simple struct pointing to a buffer of the specified length. For convenience,
 * contains a unionized const and non-const pointer to the buffer. */
struct buffer_ptr {
	union {
		char *buf;
		const char *cbuf;
	};
	size_t len;
};


// Misc ////////////////////////////////////////////////////////////////////////

/** Gets the length of the specified array. */
#define ARRSIZEOF(p_arr) (sizeof((p_arr)) / sizeof((*p_arr)))

/** Gets the pointer to a field in `cont` specified by `offset`. Uses the GNU
 * extension that allows using pointer arithmetics with `void *`. */
#define OFFSETATTR(cont, offset) ((void *) (cont)) + ((size_t) (offset))

/** Ceiling division. Calculates `a / b` and, if `(a) % (b)` is non-zero, adds
 * one to the result. Useful for calculating capacities. */
#define CEIL_DIV(a, b) ((a) / (b) + !!((a) % (b)))

/** Sets `bit` in `bitmap` to 1. */
static inline void set_bit(uint8_t *bitmap, size_t bit)
{
	bitmap[bit / 8] |= (1 << (bit % 8));
}

/** Sets `bit` in `bitmap` to 0. */
static inline void reset_bit(uint8_t *bitmap, size_t bit)
{
	bitmap[bit / 8] &= ~(1 << (bit % 8));
}

/** Checks whether `bit` in `bitmap` is 1. */
static inline bool test_bit(const uint8_t *bitmap, size_t bit)
{
	return !!(bitmap[bit / 8] & (1 << (bit % 8)));
}

/** Performs a popcount of the specified `bitmap` of byte length `len`. */
size_t popcnt_bits(const uint8_t *bitmap, size_t len);
